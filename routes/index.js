var express = require('express');
var router = express.Router();

// Mangoose 
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/Crud', {useNewUrlParser: true});
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log('MangoDB conectado!!');
});

router.get('/', function (req, res) {
  //res.render('index', {title: 'Pagina Inicial'});
  // const cursor = db.collection("Pessoa").find({}).toArray();
  findAll((e, cursor) => {
    if(e) {
      return console.log(e);
    }
    //console.log(cursor);
    res.render('index', {title: "Lista De pessoas ", cursor:cursor});
  })
  
});

function findAll(callback) {
  db.collection("Pessoa").find({}).toArray(callback); // Mostrar
}

router.post('/inserir', function (req, res) {  
  var  nome = req.body.nome;
  var idade = req.body.idade;
  findAll((e, docs) => {
    if (e) {
      return console.log(e);
    }
    let count = 0;
    for (let i = 0; i < docs.length; i++) {
      if (docs[i].nome === nome) {
        count = 1;
      }
    }

    if (count > 0) {
      console.log("Valor Duplicado!");
    } else {
      db.collection("Pessoa").insertOne({ nome, idade }, (err, r) => {
        if (err) {
          return console.log(err);
        }
      });
    }
    res.redirect("/");
  });
});


router.post('/deletar', function(req, res) {
   var nome = req.body.nome;
   var myquery = { nome : nome  };
   var id;
   db.collection("Pessoa").findOne(myquery,(e,r)=>{
    if (e) {
      return console.log(e); 
    }
    db.collection("Pessoa").deleteOne({ _id: r._id }, (e, r) => {
      if (e) {
        return console.log(e);
      };
      res.redirect("/");
    });
   }); 
  /* db.collection("Pessoa").findOneAndDelete(myquery, (e, r) => {  // Delete Utilizando Myquery ( name : name)
    if (e) {
      return console.log(e);
    }
    res.redirect("/");
  });*/
});

router.post('/editar', function(req, res) {
  var nome = req.body.newName;
  var oldName = req.body.oldName;
  var idade = parseInt(req.body.idade);
  var myquery = { nome : oldName  };
  var newvalues = {$set: {nome: nome , idade : idade } }; 
  db.collection("Pessoa").findOneAndUpdate(myquery,newvalues,(err, r) => { 
  if (err) {
      return console.log(err);
    }
    res.redirect("/"); 
  });
    
});


module.exports = router;
